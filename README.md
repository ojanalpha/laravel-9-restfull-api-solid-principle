### About Laravel Solid Principle

SOLID, this acronym was coined by Michael Feathers, it represents the five basic principles of object-oriented programming developed

Most programmers probably know this acronym. But it seems to me that a minority can decode it.

Is it wrong? In my opinion, no, I think that writing clean and simple code is more important than knowing the theory. But don't completely ignore the theory. How else will you pass on knowledge to someone? How do you justify your code in a discussion during the code review? You have to be based on theory and generally accepted standards.

But it's also good to know the basics of what clean and simple code looks like. SOLID principles can be used in any object-oriented programming language. I work in Symfony on a daily basis, so I will show some principles in PHP.

So let's go through the five SOLID principles together.

### Installation

here are the steps to use Laravel restfull api, if you have any questions please ask me ( Ojan )

```bash
git clone https://gitlab.com/ojanalpha/laravel-9-restfull-api-solid-principle.git larasolid
```
dont forgot to move the current folder directory into the previous GitLab clone folder

```bash
cd larasolid
```

please copy .env.example to be .env using the code below

```bash
cp .env.example .env
```

please install all composer dependencies

```bash
composer install
```

dont forget install passport 

```bash
php artisan passport:install
```

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
