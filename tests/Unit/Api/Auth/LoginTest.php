<?php

namespace Tests\Unit\Api\Auth;

use App\Repositories\LoginRepository\Models\LoginData;
use App\Services\LoginUser\LoginService;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_login_is_true()
    {
        $data = new LoginData;
        $data->email = 'admin@admin.com';
        $data->password = 'admin123';
        $response = (new LoginService($data))->call();
        // echo "\nresult : " . json_encode($response);
        self::assertTrue($response->status() == 200);
    }

    public function test_login_is_false()
    {
        $data = new LoginData;
        $data->email = 'admin@admin.com';
        $data->password = 'admin1233';
        $response = (new LoginService($data))->call();
        // echo "\nresult : " . json_encode($response);
        self::assertEquals(300, $response->status() == 300);
    }
}
