<?php

namespace App\Services\LoginUser;

use App\Base\ServiceBase;
use App\Repositories\LoginRepository\LoginRepository;
use App\Repositories\LoginRepository\Models\LoginData;
use App\Responses\ServiceResponse;
use Illuminate\Support\Facades\Validator;

class LoginService extends ServiceBase
{

    public function __construct(LoginData $data)
    {
        $this->data = $data;
    }

    /**
     * Validate the data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validate(): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($this->data->toArray(), [
            "email"    => "required|email",
            "password" => "required",
        ], [
            'email.required' => 'Masukan Email Anda',
            'email.email' => 'EMail Tidak Valid',
            'password' => 'Masukan Password'
        ]);
    }
    /**
     * main method of this service
     *
     * @return ServiceResponse
     */
    public function call(): ServiceResponse
    {
        if ($this->validate()->fails()) {
            return self::error($this->validate()->errors(), 'Error Validate Input');
        }

        $dataStore = (new LoginRepository)->Login($this->data);
        if ($dataStore == 300) {
            # code...
            return self::error('Incorrect Details. Please try again');
        }
        $data = [
            'token_type' => 'Bearer',
            'access_token' => $dataStore['token'],
            'user' => $dataStore['user'],
        ];
        return self::success($data);
    }
}
