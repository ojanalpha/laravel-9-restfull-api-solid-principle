<?php

namespace App\Repositories\LoginRepository;

use App\Models\User;
use App\Repositories\LoginRepository\Models\LoginData;
use Illuminate\Support\Facades\Auth;

class LoginRepository implements LoginContract
{

    public function login(LoginData $data)
    {
        if (Auth::attempt(['email' => $data->email, 'password' => $data->password])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('api-login')->accessToken;
            $success['user'] =  $user;
            return $success;
        } else {
            return 300;
        }
    }
}
