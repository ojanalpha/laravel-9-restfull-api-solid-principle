<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\LoginRepository\Models\LoginData;
use App\Services\LoginUser\LoginService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $data = new LoginData;
        $data->email = $request->email;
        $data->password = $request->password;

        return response()->json((new LoginService($data))->call());
    }
}
