<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\RegisterRepository\Models\RegisterData;
use App\Services\RegisterUser\RegisterService;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        $data = new RegisterData;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = $request->password;
        $data->current_password = $request->password;

        return response()->json((new RegisterService($data))->call());
    }
}
